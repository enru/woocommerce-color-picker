
<fieldset>
<p class="form-row">
  <input type="button" value="Choose Colour" id="woocommerce-color-picker-action"/>
</p>
<p class="form-row">
  <label for="woocommerce-color-picker-choice"><?php esc_html_e(__('Color')); ?>:</label>
  <input type="text" value="" name="woocommerce-color" id="woocommerce-color-picker-choice"/>
</p>
</fieldset>
<div id="woocommerce-color-picker-overlay" style="display: none;">
  <div id="woocommerce-color-picker-overlay-bg" class="modal"></div>
  <div id="woocommerce-color-picker-overlay-wrapper" class="modal modal-content">
    <div id="woocommerce-color-picker-overlay-content"></div>
  </div>
</div>


<?php // BEFORE template for the color picker overlay ?>
<script type="text/html" id="tmpl-woocommerce-color-picker-before-template">
Please choose a color.
</script>

<script type="text/html" id="tmpl-woocommerce-color-picker-parent-color-template">
<div class="parent-color">{{data.name}}</div>
</script>

<?php // COLOR template for the color picker overlay ?>
<?php // JS code will respond to clicks on any element with the "color-option" CSS class ?>
<script type="text/html" id="tmpl-woocommerce-color-picker-color-template">
<# if ( ! data.image && data.description) { #>
<div class="color-option color" data-color="{{data.name}}" title="{{data.name}}" style="background-color:{{data.description}}"></div>
<# } #>
<# if ( ! data.image && ! data.description) { #>
<div class="color-option name" data-color="{{data.name}}" title="{{data.name}}" >{{data.name}}</div>
<# } #>
<# if ( data.image ) { #>
<img class="color-option image" data-color="{{data.name}}" title="{{data.name}}" src="{{data.image}}" alt="{{data.name}}">{{data.name}}
<# } #>
</script>

<?php // AFTER template for the color picker overlay ?>
<script type="text/html" id="tmpl-woocommerce-color-picker-after-template">
Thanks
</script>
