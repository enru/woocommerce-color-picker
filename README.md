# README #

A WordPress plugin that adds a color field and popup to a WooCommerce product's add to cart form.

* Copy the plugin into you plugins directory and enable the plugin.
* Add colors to the newly created Product Colors taxonomy. Use the "name" field as the color's name and enter the HTML RGB code for the color into the taxonomy's "description" field. e.g. name = Red and description = #FF0000
* Edit a product and select some of the Product Colors you just created.

Now when viewing that product is will have a color option that when focused will pop up the product's colors in an overlay.

The color will be displayed in the cart, checkout and final order.

## Templates

You can alter the presentation of the color field in the add to cart form by copying the plugin's template file:

YOUR_WP_CONTENT_DIR/plugins/woocommerce-color-picker/templates/color-field-single-product.php

into 

YOUR_THEME/woocommerce/color-field-single-product.php

The presentation of the popup overlay used to display the products can be overided using CSS.

There are JavaScript templates available in color-field-single-product.php that can be used to alter the display of colors and the overlay with before and after content.
