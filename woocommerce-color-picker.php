<?php
/*
Plugin Name: Woocommerce Color Picker 
Plugin URI: http://enru.co.uk/plugins/woocommerce-color-picker/
Description: A WordPress plugin that adds a color field and popup to a WooCommerce product's add to cart form.
Author: Neill Russell 
Version: 0.2
Author URI: http://enru.co.uk/
*/

/* 
You can edit the color field template by copying:

./templates/color-field-single-product.php 

to 

YOUR_THEME/woocommerce/color-field-single-product.php
*/

class Woocommerce_Color_Picker {

	function __construct() {

		// create Product Colors taxonomy
		add_action('init', array($this, 'init_taxonomy'));

		// add field to Product Add to Cart form
		add_action('woocommerce_before_add_to_cart_button', array($this, 'inject_color_field'));

		// add item data to the cart
		add_filter('woocommerce_add_cart_item_data', array($this, 'add_cart_item_data'),10,2);

		// make sure additional data sticks in the session
		add_filter('woocommerce_get_cart_item_from_session', array($this, 'get_cart_item_from_session'), 10, 2);

		// display the data in the cart item
		add_filter('woocommerce_get_item_data', array($this, 'get_item_data'), 20, 2);

		// save the extra data to the order
		add_action('woocommerce_order_item_meta', array($this, 'order_item_meta'), 10, 20);
		add_action('woocommerce_add_order_item_meta', array($this, 'order_item_meta_2'), 10, 2);

		// set up JS & CSS
		add_action('get_header', array($this, 'init_frontend'));

		// use our templates
		add_filter( 'wc_get_template_part', array($this, 'get_template_part'), 10, 3);
	}

	// add field to Product Add to Cart form
	function inject_color_field() {
		global $post;
		$colors = wp_get_post_terms($post->ID, 'woocommerce-color-picker-colors');
		if(!is_array($colors) || count($colors) < 1) return; 

		//  load our color field
		wc_get_template_part('color-field', 'single-product');
	}


	// display the data in the cart item
	function get_item_data($other_data, $cart_item) {
		if(isset($cart_item['woocommerce-color']) && $cart_item['woocommerce-color']) {
			$other_data[] = array('name' => 'Color', 'value' => $cart_item['woocommerce-color']);
		}   
		return $other_data;
	}


	// add item data to the cart
	function add_cart_item_data($cart_item_data, $product_id) {
		if(isset($_POST['woocommerce-color'])) {
			$cart_item_data['woocommerce-color'] = $_POST['woocommerce-color'];
		}
		return $cart_item_data;	
	}

	// make sure additional data sticks in the session
	function get_cart_item_from_session($cart_item, $values) {
		if (isset($values['woocommerce-color'])) {
			$cart_item['woocommerce-color'] = $values['woocommerce-color'];
		}   
		return $cart_item;
	}


	// save the extra data to the order
	function order_item_meta($item_meta, $cart_item) {
	    if(isset($cart_item['woocommerce-color']) && $cart_item['woocommerce-color']) {
		$item_meta->add('Color', $cart_item['woocommerce-color']);
	    }   
	}

	// save the extra data to the order WC 2
	function order_item_meta_2($item_id, $values) {
		if (function_exists('woocommerce_add_order_item_meta')) {
		    if(isset($values['woocommerce-color']) && $values['woocommerce-color']) {
			woocommerce_add_order_item_meta($item_id, 'Color', $values['woocommerce-color']);
		    }   
		}
	}

	// sets up our environment - to be called thru an action
	function init_frontend() {
		if(is_product()) {
			add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));
			wp_register_style('woocommerce-color-picker', plugins_url('assets/css/woocommerce-color-picker.css', __FILE__));
			wp_enqueue_style('woocommerce-color-picker');
		}
	}

	// set up JS files & vars used by this plugin
	function enqueue_scripts() {

		// embed the javascript file
		wp_enqueue_script(
			'woocommerce-color-picker',
			plugin_dir_url(__FILE__) . 'assets/js/woocommerce-color-picker.js',
			array('jquery', 'wp-util'));

		// declare the URL to the file that handles the AJAX request (wp-admin/admin-ajax.php)

		global $post;

		$parents = get_terms('woocommerce-color-picker-colors', array(
			'parent' => 0,
		));

		$colors = wp_get_object_terms($post->ID, 'woocommerce-color-picker-colors', array(
			//'orderby' => 'term_order'
		));
		
		// allow themes/plugins to alter colors
		$colors = apply_filters('woocommerce-color-picker-colors', $colors);

		$rainbow = array_map(function($item) {
			$color = array_intersect_key((array) $item, array('term_id' => '', 'name'=>'', 'description'=>'', 'parent' => ''));
			if(function_exists('get_field')) {
				if($image = get_field('image', 'woocommerce-color-picker-colors_'.$item->term_id)) {
					$color['image'] = $image;
				}
			}
			return apply_filters('woocommerce-color-picker-color', $color);
		}, $colors);

		$colors = array();
		// populate the parent colors
		foreach($parents as $parent) $colors[$parent->name] = array();

		foreach($rainbow as $color) {

			// check if the color's parent has kids
			$kids = get_term_children($color['parent'], 'woocommerce-color-picker-colors');
			if(!is_wp_error($kids) && count($kids) > 0) {
				// if it does & it's a real term...
				$parent = get_term($color['parent'], 'woocommerce-color-picker-colors');
				if($parent && !is_wp_error($parent)) { 
					// add the color to the parent array
					array_push($colors[$parent->name], $color);
				}
			}
			// the color has no parent so add the color at the top level 
			else {
				// only add if this has no children - parents should have been handled above
				$kids = get_term_children($color['term_id'], 'woocommerce-color-picker-colors');
				if(!is_wp_error($kids) && count($kids) > 0) { }
				else {
					if(!isset($colors[$color['name']])) $colors[$color['name']] = array();
					array_push($colors[$color['name']], $color);
				}
			}
		}

		// check if any parent colors have more than one child color
		$multi = false;
		foreach($colors as $parent => $kids) {
			if(count($kids) > 1) { $multi=true; break; }
		}

		// if they do then reduce them to one array
		if($multi == false) {
			foreach($colors as $parent => $kids) {
				foreach($kids as $color) $colors[''][] = $color;
				unset($colors[$parent]);
			}
		}

		$args = array(
			'ajaxurl' => admin_url('admin-ajax.php'),
			'colors' => $colors,
		);

		wp_localize_script(
			'woocommerce-color-picker',// script handle
			'woocommerce_color_picker',// variable name
			$args
		);
	}

	// locate our own templates
	function get_template_part($template, $slug, $name) {
		// load our template if not found already 
		if($template == '' && $slug == 'color-field' &&  $name == 'single-product') {
			$template = plugin_dir_path(__FILE__).'templates/color-field-single-product.php';
		}
		return $template;
	}

	// create the taxonomy for colors
	function init_taxonomy() {

		// create a new taxonomy
		register_taxonomy(
			'woocommerce-color-picker-colors',
			array('product'),
			array(
				'label' => __( 'Product Colors' ),
				'labels' => array(
					'name'              => _x( 'Product Colors', 'taxonomy general name' ),
					'singular_name'     => _x( 'Product Color', 'taxonomy singular name' ),
					'search_items'      => __( 'Search Product Colors' ),
					'all_items'         => __( 'All Product Colors' ),
					'parent_item'       => __( 'Parent Product Color' ),
					'parent_item_colon' => __( 'Parent Product Color:' ),
					'edit_item'         => __( 'Edit Product Color' ),
					'update_item'       => __( 'Update Product Color' ),
					'add_new_item'      => __( 'Add New Product Color' ),
					'new_item_name'     => __( 'New Product Color' ),
					'menu_name'         => __( 'Product Colors' ),
				),
				'rewrite' => array( 'slug' => 'woocommerce-color-picker-colors' ),
				'capabilities' => array(),
				'hierarchical' => true,
			)
		);
	}

}

global $woocommerce_color_picker;
$woocommerce_color_picker = new Woocommerce_Color_Picker();
