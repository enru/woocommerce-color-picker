var resizeOverlay = function($) {
    // fix overlay vertical position
    var wh = window.innerHeight;
    var oh = $('div#woocommerce-color-picker-overlay-wrapper').height();
    var h = Math.round((wh-oh)/2);
    $('div#woocommerce-color-picker-overlay-wrapper').css('top', h+'px');
}

var hideOverlay = function() {
  jQuery('#woocommerce-color-picker-overlay-content').empty();
  jQuery('#woocommerce-color-picker-overlay').hide();
}

jQuery(function($) {

  // if a color is picked the put it into the input box
  $('#woocommerce-color-picker-overlay').bind('click', function(e) {
    if($(e.target).hasClass('color-option')) {
      e.stopPropagation();
      var color = $(e.target).attr('data-color');
      $('#woocommerce-color-picker-choice').val(color);
      hideOverlay();
    }
  });

  $('#woocommerce-color-picker-action').bind('click', function() {

    if($('#woocommerce-color-picker-overlay').length > 0) {

      // close button/action
      $('#woocommerce-color-picker-overlay-bg, #woocommerce-color-picker-overlay-close').bind('click', function(e) {
        hideOverlay();
      });

      // create the pallet
      var colorTemplate = wp.template('woocommerce-color-picker-color-template');
      var parentColorTemplate = wp.template('woocommerce-color-picker-parent-color-template');
      var colorHTML = '';
      var colors = woocommerce_color_picker.colors; 

/*
      if(colors.length > 0) {
        colors.forEach(function(c) {
          colorHTML += colorTemplate(c);
        });
      }
*/
      for(var parentColor in colors) {
        if(colors[parentColor].length > 0) {
          colorHTML += parentColorTemplate({name: parentColor});
          colors[parentColor].forEach(function(c) {
            colorHTML += colorTemplate(c);
          });
        }
      }

      var beforeTemplate = wp.template('woocommerce-color-picker-before-template');
      var afterTemplate = wp.template('woocommerce-color-picker-after-template');

      var content = $('<div></div>').html(
        '<div class="before">'+beforeTemplate()+'</div>'+
        '<div class="colors">'+colorHTML+'</div>'+
        '<div class="after">'+afterTemplate()+'</div>');

      // add colors to overlay
      if(content.length) {
        $('#woocommerce-color-picker-overlay-wrapper').attr('style', '');
        $('#woocommerce-color-picker-overlay-content').html(content);
        $('#woocommerce-color-picker-overlay').show();
	//resizeOverlay($);
      }

    }
  });

  //$( window ).resize(function() {
    //resizeOverlay($);
  //});

});
